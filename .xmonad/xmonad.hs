--------------------------------------------------------------------------------
-- | Example.hs
--
-- Example configuration file for xmonad using the latest recommended
-- features (e.g., 'desktopConfig').
module Main (main) where

--------------------------------------------------------------------------------
import System.Exit
import XMonad
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.BinarySpacePartition (emptyBSP)
import XMonad.Layout.NoBorders (noBorders)
import XMonad.Layout.NoFrillsDecoration
import XMonad.Layout.ResizableTile (ResizableTall(..), MirrorResize(..))
import XMonad.Layout.Spacing
import XMonad.Layout.ToggleLayouts (ToggleLayout(..), toggleLayouts)
import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt
import XMonad.Prompt.Shell
import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
-- Import Pywal generated colors
import Colors

--------------------------------------------------------------------------------
main :: IO ()
main = do
  -- Start xmonad using the main desktop configuration with a few
  -- simple overrides:
  xmonad $ ewmh $ desktopConfig
    { modMask    = mod4Mask -- Use the "Win" key for the mod key
    , terminal   = myTerminal
    , normalBorderColor  = myNormalBorderColor
    , focusedBorderColor = myFocusedBorderColor
    , borderWidth        = myBorderWidth
    , workspaces  = myWorkspaces
    , manageHook  = myManageHook <+> manageHook desktopConfig
    , layoutHook  = desktopLayoutModifiers $ myLayouts
    , startupHook = myStartupHook
    , logHook     = dynamicLogString def >>= xmonadPropLog
    }

    `additionalKeysP` -- Add some extra key bindings:
      [
      -- , ("M-S-q",   confirmPrompt myXPConfig "exit" (io exitSuccess))
      -- , ("M-p",     shellPrompt myXPConfig)
        ("M-a", sendMessage (MirrorShrink))
      , ("M-z", sendMessage (MirrorExpand))
      , ("M-C-l", spawn "xset s activate")
      , ("M-r", spawn myLauncher)
      , ("M-d", spawn myFileManager)
      , ("M-f", sendMessage (Toggle "Full") <+> sendMessage (ToggleStruts))
      ]

--------------------------------------------------------------------------------
-- | Customize ....
--
-- Terminal
-- The preferred terminal program
myTerminal :: [Char]
-- myTerminal = "konsole"
myTerminal = "alacritty"

myLauncher :: [Char]
myLauncher = "rofi -show combi"

myFileManager :: [Char]
myFileManager = "dolphin"
--------------------------------------------------------------------------------
-- | Customize layouts.
--
-- This layout configuration uses two primary layouts, 'ResizableTall'
-- and 'BinarySpacePartition'.  You can also use the 'M-<Esc>' key
-- binding defined above to toggle between the current layout and a
-- full screen layout.
myLayouts = toggleLayouts (noBorders Full) (spacingRaw True (Border 10 10 10 10) True (Border 10 10 10 10) True $ others)
  where
    others = ResizableTall 1 (1.5/100) (3/5) [] |||
             Mirror (ResizableTall 1 (1.5/100) (3/5) []) |||
             noFrillsDeco shrinkText topBarTheme emptyBSP

-- Theme for window decoration
--
myNormalBorderColor :: [Char]
myNormalBorderColor = color0

myFocusedBorderColor :: [Char]
myFocusedBorderColor = color7
-- Width of the window border in pixels.
myBorderWidth :: Dimension
myBorderWidth = 8


topBarTheme :: Theme
topBarTheme = def
    { fontName              = "-*-terminus-medium-*-*-*-*-160-*-*-*-*-*-*"
    , inactiveBorderColor   = "#002b36"
    , inactiveColor         = "#002b36"
    , inactiveTextColor     = "#002b36"
    , activeBorderColor     = "#268bd2"
    , activeColor           = "#268bd2"
    , activeTextColor       = "#268bd2"
    , urgentBorderColor     = "#dc322f"
    , urgentTextColor       = "#b58900"
    , decoHeight            = 10
}

--------------------------------------------------------------------------------
-- | Customize the way 'XMonad.Prompt' looks and behaves.  It's a
-- great replacement for dzen.
-- myXPConfig = def
--   { position          = Top
--   , alwaysHighlight   = True
--   , promptBorderWidth = 0
--   , font              = "xft:monospace:size=9"
--   }

------------------------------------------------------------------------
-- Workspaces
-- The default number of workspaces (virtual screens) and their names.
--
myWorkspaces :: [[Char]]
myWorkspaces = ["1:chat","2:emacs","3:code","4:web","5:mail"] ++ map show [6..9]


--------------------------------------------------------------------------------
-- | Manipulate windows as they are created.  The list given to
-- @composeOne@ is processed from top to bottom.  The first matching
-- rule wins.
--
-- Use the `xprop' tool to get the info you need for these matches.
-- For className, use the second value that xprop gives you.
myManageHook :: ManageHook
myManageHook = composeOne
  [ className =? "konversation"   -?> doShift "1:chat"
  , className =? "Emacs"          -?> doShift "2:emacs"
  , className =? "Chromium"       -?> doShift "4:web"
  , className =? "Google-chrome"  -?> doShift "4:web"
  , className =? "Firefox"        -?> doShift "4:web"
  , className =? "Opera"          -?> doShift "4:web"
  , className =? "Thunderbird"    -?> doShift "5:mail"
  , className =? "Bricscad"       -?> doShift "8"
  , title     =? "LookFrom"       -?> doIgnore
  , title     =? "Open Drawing"   -?> doFloat
  , title     =? "Unlock Database - KeePassXC"   -?> doFloat
  , resource  =? "desktop_window" -?> doIgnore
  , resource  =? "ksmserver"      -?> doFloat
  , className =? "lattedock"      -?> doFloat
  , className =? "albert"         -?> doCenterFloat
  , className =? "Citrix_Firefox" -?> doShift "9"
  , className =? "Wfica"          -?> doFloat
  , className =? "Wfica_Seamless" -?> doFloat
  , className =? "Xephyr"         -?> doShift "8"
  , isDialog                      -?> doCenterFloat
  , isInProperty "_NET_WM_WINDOW_TYPE" "_KDE_NET_WM_WINDOW_TYPE_OVERRIDE" -?> doFloat
    -- Move transient windows to their parent:
  , transience
  ]

--------------
-- satrtUpHook
--------------
myStartupHook = do
  spawn "wal -R"
  spawn "polybar_start"
